package org.techern.minecraft.extrapaths.blocks;

import com.google.common.collect.ImmutableList;
import com.mojang.datafixers.util.Pair;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.LootTableProvider;
import net.minecraft.data.loot.BlockLootTables;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class PathBlockLootProvider extends LootTableProvider {
    public PathBlockLootProvider(DataGenerator p_i50789_1_) {
        super(p_i50789_1_);
    }

    @Override
    protected List<Pair<Supplier<Consumer<BiConsumer<ResourceLocation, LootTable.Builder>>>, LootParameterSet>> getTables() {
        return ImmutableList.of(
                Pair.of(BlockLoot::new, LootParameterSets.BLOCK)
        );
    }

    @Override
    protected void validate(Map<ResourceLocation, LootTable> map, ValidationResults validationresults) {
        map.forEach((name, table) -> LootTableManager.func_215302_a(validationresults, name, table, map::get));
    }

    private static class BlockLoot extends BlockLootTables {
        @Override
        protected void addTables() {
            for (PathBlock block : PathBlock.BLOCKS.values()) {
                switch (block.getParent().getRegistryName().getPath()) {
                    case "stone":
                        registerLootTable(block, LootTable.builder().addLootPool(func_218560_a(block, LootPool.builder().rolls(ConstantRange.of(1)).addEntry(ItemLootEntry.builder(Blocks.COBBLESTONE)))));
                        break;
                    default:
                        registerLootTable(block, LootTable.builder().addLootPool(func_218560_a(block, LootPool.builder().rolls(ConstantRange.of(1)).addEntry(ItemLootEntry.builder(block.getParent())))));
                        break;

                }

            }
            for (GlowingPathBlock block : PathBlock.GLOWING_BLOCKS.values()) {
                switch (block.getParent().getRegistryName().getPath()) {
                    case "stone":
                        registerLootTable(block, LootTable.builder().addLootPool(func_218560_a(block, LootPool.builder().rolls(ConstantRange.of(1)).addEntry(ItemLootEntry.builder(Blocks.COBBLESTONE)))));
                        break;
                    default:
                        registerLootTable(block, LootTable.builder().addLootPool(func_218560_a(block, LootPool.builder().rolls(ConstantRange.of(1)).addEntry(ItemLootEntry.builder(block.getParent())))));
                        break;

                }

            }
        }

        @Override
        protected Iterable<Block> getKnownBlocks() {
            ArrayList<Block> list = new ArrayList<>(PathBlock.BLOCKS.size());

            list.addAll(PathBlock.BLOCKS.values());
            list.addAll(PathBlock.GLOWING_BLOCKS.values());
            return list;
        }
    }
}
