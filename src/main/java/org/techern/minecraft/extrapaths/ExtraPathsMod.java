package org.techern.minecraft.extrapaths;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.data.DataGenerator;
import net.minecraft.item.*;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.techern.minecraft.extrapaths.blocks.*;
import org.techern.minecraft.extrapaths.items.PathItemModelProvider;
import org.techern.minecraft.extrapaths.lang.ExtraPathsLanguageProvider;

import java.io.IOException;

@Mod("extrapaths")
public class ExtraPathsMod {

    /**
     * The {@link Logger}. Seriously, what did you expect?
     *
     * @since 1.0.0
     */
    public static final Logger LOGGER = LogManager.getLogger();

    /**
     * The {@link ItemGroup} for all paths
     *
     * TODO Do we move grass paths in here or remake them?
     * @since 1.0.0
     */
    public static final ItemGroup ITEM_GROUP = new ItemGroup("pathGroup") {

        /**
         * Creates the {@link ItemStack} icon but only in the client
         *
         * @return The icon represented as a {@link ItemStack}
         * @since 1.0.0
         */
        @Override
        @OnlyIn(Dist.CLIENT)
        public ItemStack createIcon() {
            return new ItemStack(Items.GRASS_PATH);
        }
    };

    /**
     * Constructs the {@link ExtraPathsMod}
     *
     * @since 1.0.0
     */
    public ExtraPathsMod() {
        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
        MinecraftForge.EVENT_BUS.register(new BlockInteraction());

        //Ok, start registering stone blocks
        PathBlock.BLOCKS.put(Blocks.COBBLESTONE, new PathBlock(Blocks.COBBLESTONE, "cobblestone_path", "block/cobblestone"));
        PathBlock.BLOCKS.put(Blocks.STONE, new PathBlock(Blocks.STONE, "stone_path", "block/stone"));
        PathBlock.BLOCKS.put(Blocks.SMOOTH_STONE, new PathBlock(Blocks.SMOOTH_STONE, "smooth_stone_path", "block/smooth_stone"));
        PathBlock.BLOCKS.put(Blocks.ANDESITE, new PathBlock(Blocks.ANDESITE, "andesite_path", "block/andesite"));
        PathBlock.BLOCKS.put(Blocks.POLISHED_ANDESITE, new PathBlock(Blocks.POLISHED_ANDESITE, "polished_andesite_path", "block/polished_andesite"));
        PathBlock.BLOCKS.put(Blocks.GRANITE, new PathBlock(Blocks.GRANITE, "granite_path", "block/granite"));
        PathBlock.BLOCKS.put(Blocks.POLISHED_GRANITE, new PathBlock(Blocks.POLISHED_GRANITE, "polished_granite_path", "block/polished_granite"));
        PathBlock.BLOCKS.put(Blocks.DIORITE, new PathBlock(Blocks.DIORITE, "diorite_path", "block/diorite"));
        PathBlock.BLOCKS.put(Blocks.POLISHED_DIORITE, new PathBlock(Blocks.POLISHED_DIORITE, "polished_diorite_path", "block/polished_diorite"));
        PathBlock.BLOCKS.put(Blocks.PRISMARINE, new PathBlock(Blocks.PRISMARINE, "prismarine_path", "block/prismarine"));
        PathBlock.BLOCKS.put(Blocks.PRISMARINE_BRICKS, new PathBlock(Blocks.PRISMARINE_BRICKS, "prismarine_brick_path", "block/prismarine_bricks"));
        PathBlock.BLOCKS.put(Blocks.DARK_PRISMARINE, new PathBlock(Blocks.DARK_PRISMARINE, "dark_prismarine_path", "block/dark_prismarine"));


        //Now we do... Sand and Sandstone
        PathBlock.BLOCKS.put(Blocks.SAND, new PathBlock(Blocks.SAND, "sand_path", "block/sand"));
        PathBlock.BLOCKS.put(Blocks.RED_SAND, new PathBlock(Blocks.RED_SAND, "red_sand_path", "block/red_sand"));
        PathBlock.BLOCKS.put(Blocks.SOUL_SAND, new PathBlock(Blocks.SOUL_SAND, "soul_sand_path", "block/soul_sand")); //Why the hell not lol

        PathBlock.BLOCKS.put(Blocks.SANDSTONE, new PathBlock(Blocks.SANDSTONE, "sandstone_path",
                "block/sandstone_top", "block/sandstone_bottom",
                "block/sandstone"));
        PathBlock.BLOCKS.put(Blocks.SMOOTH_SANDSTONE, new PathBlock(Blocks.SMOOTH_SANDSTONE, "smooth_sandstone_path","block/sandstone_top"));
        PathBlock.BLOCKS.put(Blocks.CUT_SANDSTONE, new PathBlock(Blocks.CUT_SANDSTONE, "cut_sandstone_path",
                "block/sandstone_top", "block/sandstone_bottom",
                "block/cut_sandstone"));
        PathBlock.BLOCKS.put(Blocks.CHISELED_SANDSTONE, new PathBlock(Blocks.CHISELED_SANDSTONE, "chiseled_sandstone_path",
                "block/sandstone_top", "block/sandstone_bottom",
                "block/chiseled_sandstone"));

        PathBlock.BLOCKS.put(Blocks.RED_SANDSTONE, new PathBlock(Blocks.RED_SANDSTONE, "red_sandstone_path",
                "block/red_sandstone_top", "block/red_sandstone_bottom",
                "block/red_sandstone"));
        PathBlock.BLOCKS.put(Blocks.SMOOTH_RED_SANDSTONE, new PathBlock(Blocks.SMOOTH_RED_SANDSTONE, "smooth_red_sandstone_path","block/red_sandstone_top"));
        PathBlock.BLOCKS.put(Blocks.CUT_RED_SANDSTONE, new PathBlock(Blocks.CUT_RED_SANDSTONE, "cut_red_sandstone_path",
                "block/red_sandstone_top", "block/red_sandstone_bottom",
                "block/cut_red_sandstone"));
        PathBlock.BLOCKS.put(Blocks.CHISELED_RED_SANDSTONE, new PathBlock(Blocks.CHISELED_RED_SANDSTONE, "chiseled_red_sandstone_path",
                "block/red_sandstone_top", "block/red_sandstone_bottom",
                "block/chiseled_red_sandstone"));


        //Now we do... Plonks!
        PathBlock.BLOCKS.put(Blocks.ACACIA_PLANKS, new PathBlock(Blocks.ACACIA_PLANKS, "acacia_plank_path","block/acacia_planks"));
        PathBlock.BLOCKS.put(Blocks.BIRCH_PLANKS, new PathBlock(Blocks.BIRCH_PLANKS, "birch_plank_path","block/birch_planks"));
        PathBlock.BLOCKS.put(Blocks.DARK_OAK_PLANKS, new PathBlock(Blocks.DARK_OAK_PLANKS, "dark_oak_plank_path","block/dark_oak_planks"));
        PathBlock.BLOCKS.put(Blocks.JUNGLE_PLANKS, new PathBlock(Blocks.JUNGLE_PLANKS, "jungle_plank_path","block/jungle_planks"));
        PathBlock.BLOCKS.put(Blocks.OAK_PLANKS, new PathBlock(Blocks.OAK_PLANKS, "oak_plank_path","block/oak_planks"));
        PathBlock.BLOCKS.put(Blocks.SPRUCE_PLANKS, new PathBlock(Blocks.SPRUCE_PLANKS, "spruce_plank_path","block/spruce_planks"));


        //TODO Add more blocks here



        //Okay, finally we add all of the GLOWING blocks

        for (PathBlock block : PathBlock.BLOCKS.values()) {
            PathBlock.GLOWING_BLOCKS.put(block, new GlowingPathBlock(block));
        }


    }


    /**
     * A container class for all of our {@link RegistryEvent}s
     *
     * @since 1.0.0
     */
    @Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {


        /**
         * Called when gathering or generating data
         *
         * @param event The {@link GatherDataEvent}
         * @since 1.0.0
         */
        @SubscribeEvent
        public static void gatherData(GatherDataEvent event) {

            DataGenerator generator = event.getGenerator();

            generator.addProvider(new ExtraPathsLanguageProvider(generator, "en_us"));
            generator.addProvider(new PathBlockLootProvider(generator));

            if (event.includeClient()) {
                generator.addProvider(new PathBlockStateProvider(generator, event.getExistingFileHelper()));
                generator.addProvider(new PathItemModelProvider(generator, event.getExistingFileHelper()));
                LOGGER.info("Gathered client data");
            }
            if (event.includeServer()) {
                LOGGER.info("Gathered server data");
            }

            try {
                generator.run();
            } catch (IOException e) {
                LOGGER.error("Error generating data" , e);
            }
        }

        /**
         * Called when Forge is registering {@link Block}s
         *
         * @param blockRegistryEvent The event
         * @since 1.0.0
         */
        @SubscribeEvent
        public static void onBlocksRegistry(final RegistryEvent.Register<Block> blockRegistryEvent) {
            PathBlock.BLOCKS.values().forEach(block -> blockRegistryEvent.getRegistry().register(block));
            PathBlock.GLOWING_BLOCKS.values().forEach(block -> blockRegistryEvent.getRegistry().register(block));
            LOGGER.info("Registered " + PathBlock.BLOCKS.size() + PathBlock.GLOWING_BLOCKS.size() + " blocks!");
        }

        /**
         * Called when Forge is registering {@link Item}s
         *
         * @param itemRegistryEvent The event
         * @since 1.0.0
         */
        @SubscribeEvent
        public static void onItemsRegistry(final RegistryEvent.Register<Item> itemRegistryEvent) {
            PathBlock.BLOCKS.values().forEach(pathBlocks -> {
                BlockItem item = pathBlocks.generateBlockItem();
                PathBlock.ITEM_BLOCKS.add(item);
                itemRegistryEvent.getRegistry().register(item);
            });
            PathBlock.GLOWING_BLOCKS.values().forEach(pathBlocks -> {
                BlockItem item = pathBlocks.generateBlockItem();
                PathBlock.ITEM_BLOCKS.add(item);
                itemRegistryEvent.getRegistry().register(item);
            });
            LOGGER.info("Registered " + PathBlock.ITEM_BLOCKS.size() + " items!");
        }
    }

}
