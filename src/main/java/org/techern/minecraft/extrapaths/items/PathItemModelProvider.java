package org.techern.minecraft.extrapaths.items;

import net.minecraft.data.DataGenerator;
import net.minecraftforge.client.model.generators.ExistingFileHelper;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import org.techern.minecraft.extrapaths.blocks.PathBlock;

public class PathItemModelProvider extends ItemModelProvider {


    public PathItemModelProvider(DataGenerator generator, ExistingFileHelper existingFileHelper) {
        super(generator, "extrapaths", existingFileHelper);
    }

    @Override
    protected void registerModels() {
        for (PathBlock block : PathBlock.BLOCKS.values()) {
            String path = block.getRegistryName().getPath();
            getBuilder(path).parent(new ModelFile.UncheckedModelFile(modLoc("block/" + path)));
        }
        for (PathBlock block : PathBlock.GLOWING_BLOCKS.values()) {
            String path = block.getRegistryName().getPath();
            getBuilder(path).parent(new ModelFile.UncheckedModelFile(modLoc("block/" + path)));
        }
    }

    @Override
    public String getName() {
        return "extraPathsPathItemModelProvider";
    }
}
