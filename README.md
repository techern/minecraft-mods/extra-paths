# Extra Paths
A Minecraft 1.14.4 mod, built against Forge version 28.1.103

## What does it do?
It adds a variety of new paths, including stone and dirt paths. More will be added in the future!

## Can I use it in my modpack?

Yes! Please do. We prefer that you use the Curse / Twitch network but you can do whatever you want

## Can I use your mod in a video / screenshot / stream?

Sure! If you could send me an email at cruz@techern.org I'll be sure to watch it, too. Just mention the mod in the subject :)

### Why don't you have ```insert name here``` paths?

It's a feature, not a bug. We may have forgotten or just overlooked it. Maybe it's stupid? Who wants gold ore paths?

If we don't feel comfortable including it in this particular mod we can always create a fork just for you with that block - Unless it's a mod block. We're not operating at that level yet, sorry