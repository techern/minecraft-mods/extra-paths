package org.techern.minecraft.extrapaths.blocks;

import net.minecraft.block.*;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import org.techern.minecraft.extrapaths.ExtraPathsMod;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A {@link Block} that takes another {@link Block}'s {@link net.minecraft.block.Block.Properties} and steals the data
 *
 *
 * @since 1.0.0
 */
public class PathBlock extends Block {

    /**
     * The map of {@link PathBlock}s we currently have
     *
     * @since 1.0.0
     */
    public static Map<Block, PathBlock> BLOCKS = new ConcurrentHashMap<>(10);

    /**
     * The map of {@link GlowingPathBlock} we currently have
     *
     * @since 1.0.0
     */
    public static Map<PathBlock, GlowingPathBlock> GLOWING_BLOCKS = new ConcurrentHashMap<>(10);

    /**
     * The array list of {@link BlockItem} we currently have
     *
     * @since 1.0.0
     */
    public static ArrayList<BlockItem> ITEM_BLOCKS = new ArrayList<>(10);

    /**
     * The side texture name
     *
     * @since 1.0.0
     */
    private String sideTexture;

    /**
     * The top texture name
     *
     * @since 1.0.0
     */
    private String topTexture;

    /**
     * The bottom texture name
     *
     * @since 1.0.0
     */
    private String bottomTexture;

    /**
     * The {@link Block} that is the parent of this {@link PathBlock}
     *
     * @since 1.0.0
     */
    private Block parent;

    /**
     * Generates a {@link BlockItem} for a {@link PathBlock}
     *
     * @return The {@link BlockItem}
     *
     * @since 1.0.0
     */
    public BlockItem generateBlockItem() {
        return (BlockItem) (new BlockItem(this, new Item.Properties().group(ExtraPathsMod.ITEM_GROUP)).setRegistryName(this.getRegistryName())); //Yes we have to cast
    }


    /**
     * Gets the {@link Block} that is the parent of this {@link PathBlock}
     *
     * @since 1.0.0
     * @return The parent {@link Block}
     */
    public Block getParent() {
        return parent;
    }

    /**
     * Returns the side texture name
     *
     * @return The texture name
     * @since 1.0.0
     */
    public String getSideTexture() {
        return sideTexture;
    }

    /**
     * Returns the top texture name
     *
     * @return The texture name
     * @since 1.0.0
     */
    public String getTopTexture() {
        return topTexture;
    }

    /**
     * Returns the bottom texture name
     *
     * @return The texture name
     * @since 1.0.0
     */
    public String getBottomTexture() {
        return bottomTexture;
    }

    /**
     * Creates a new {@link PathBlock} while stealing the {@code parentBlock}'s {@link net.minecraft.block.Block.Properties}
     *
     * @param parentBlock The parent {@link Block}
     * @param registryName The name to use in the {@link net.minecraft.util.registry.Registry}
     * @param textureName The name of the texture to use in generated data
     * @since 1.0.0
     */
    public PathBlock(Block parentBlock, String registryName, String textureName) {
        this(parentBlock, registryName, textureName, textureName, textureName);
    }

    /**
     * Creates a new {@link PathBlock} while stealing the {@code parentBlock}'s {@link net.minecraft.block.Block.Properties}
     *
     * @param parentBlock The parent {@link Block}
     * @param registryName The name to use in the {@link net.minecraft.util.registry.Registry}
     * @param topTexture The name of the texture to use in generated data
     * @param bottomTexture The name of the texture to use in generated data
     * @param sideTexture The name of the texture to use in generated data
     * @since 1.0.0
     */
    public PathBlock(Block parentBlock, String registryName, String topTexture, String bottomTexture, String sideTexture) {
        super(Properties.from(parentBlock));

        this.parent = parentBlock;
        this.sideTexture = sideTexture;
        this.topTexture = topTexture;
        this.bottomTexture = bottomTexture;
        this.setRegistryName(registryName);

    }

    /**
     * Creates a new {@link PathBlock} while stealing the {@code parentBlock}'s {@link net.minecraft.block.Block.Properties}
     *
     * @param forcedProperties The {@link net.minecraft.block.Block.Properties} forced upon us
     * @param parentBlock The parent {@link Block}
     * @param registryName The name to use in the {@link net.minecraft.util.registry.Registry}
     * @param topTexture The name of the texture to use in generated data
     * @param bottomTexture The name of the texture to use in generated data
     * @param sideTexture The name of the texture to use in generated data
     * @since 1.0.0
     */
    public PathBlock(Properties forcedProperties, Block parentBlock, String registryName, String topTexture, String bottomTexture, String sideTexture) {
        super(forcedProperties);

        this.parent = parentBlock;
        this.sideTexture = sideTexture;
        this.topTexture = topTexture;
        this.bottomTexture = bottomTexture;
        this.setRegistryName(registryName);

    }

    /**
     * Check to see if we can place this {@link PathBlock}
     *
     * @param state The state of an unknown block, probably this block after we place it
     * @param worldIn The {@link IWorldReader} we are basing our current world on
     * @param pos The {@link BlockPos} at which we are placing our block
     * @return {@code true} if we can place it, otherwise {@code false}
     *
     * @since 1.0.0
     */
    @Deprecated
    @Override
    public boolean isValidPosition(BlockState state, IWorldReader worldIn, BlockPos pos) {
        BlockState stateAbove = worldIn.getBlockState(pos.up());

        if (stateAbove.getBlock() instanceof FenceBlock) { //TODO Add more checks
            return false;
        } else {
            return true;
        }
    }

    /**
     * Gets the {@link VoxelShape} of this {@link PathBlock}
     *
     * @param state The {@link BlockState} of this {@link PathBlock}
     * @param worldIn The {@link IBlockReader} this {@link PathBlock} is located in
     * @param pos The current {@link BlockPos}
     * @param context The {@link ISelectionContext} surrounding this block
     * @return A {@link VoxelShape}
     * @since 1.0.0
     */
    @Deprecated
    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 15.0D, 16.0D);
    }
}
