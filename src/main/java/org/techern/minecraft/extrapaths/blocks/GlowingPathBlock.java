package org.techern.minecraft.extrapaths.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * A {@link PathBlock} that glows in the dark
 *
 * @since 1.0.0
 */
public class GlowingPathBlock extends PathBlock {

    /**
     * The backing {@link PathBlock}
     *
     * @since 1.0.0
     */
    private PathBlock pathBlock;

    /**
     * Gets the backing {@link PathBlock}
     *
     * @return The backing {@link PathBlock}
     * @since 1.0.0
     */
    public PathBlock getPathBlock() {
        return pathBlock;
    }

    /**
     * Creates a new {@link GlowingPathBlock}
     *
     * @param pathBlock The {@link PathBlock} to associate with this {@link GlowingPathBlock}
     * @since 1.0.0
     */
    public GlowingPathBlock(PathBlock pathBlock) {
        super(Properties.from(pathBlock.getParent()).lightValue(13), pathBlock.getParent(), pathBlock.getRegistryName() + "_glowing", pathBlock.getTopTexture(), pathBlock.getBottomTexture(), pathBlock.getSideTexture());

        this.pathBlock = pathBlock;
    }

    /**
     * Perform side-effects from block dropping, such as creating silverfish
     * TODO Fix javadoc before 1.1.0. If you see this raise a bug, I'm too tired :P
     */
    @Deprecated
    public void spawnAdditionalDrops(BlockState state, World worldIn, BlockPos pos, ItemStack stack) {
        spawnAsEntity(worldIn, pos, new ItemStack(Items.GLOWSTONE_DUST, 1));
    }

}
