package org.techern.minecraft.extrapaths.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;
import org.techern.minecraft.extrapaths.ExtraPathsMod;

/**
 * Handles right-click interaction with {@link net.minecraft.block.Block}s to become {@link PathBlock}s and {@link PathBlock}s to become {@link GlowingPathBlock}s
 *
 * @since 1.0.0
 */
@Mod.EventBusSubscriber
public class BlockInteraction {

    /**
     * Handles right-click interaction with {@link net.minecraft.block.Block}s to become {@link PathBlock}s and {@link PathBlock}s to become {@link GlowingPathBlock}s
     *
     * @param event The {@link net.minecraftforge.event.entity.player.PlayerInteractEvent.RightClickBlock} event
     * @since 1.0.0
     */
    @SubscribeEvent
    public static void makePathInteraction(PlayerInteractEvent.RightClickBlock event) {

        ItemStack heldItemStack = event.getPlayer().getHeldItem(event.getHand());
        BlockState state = event.getWorld().getBlockState(event.getPos());

        //first manually check for Prismarine...
        if (heldItemStack.getItem().getToolTypes(heldItemStack).contains(ToolType.PICKAXE)) {
            for (PathBlock type : PathBlock.BLOCKS.values()) {
                if (state.getBlock().equals(type.getParent())) {
                    if (state.getBlock().equals(Blocks.PRISMARINE) || state.getBlock().equals(Blocks.PRISMARINE_BRICKS) || state.getBlock().equals(Blocks.DARK_PRISMARINE)) {
                        event.getPlayer().swingArm(event.getHand());
                        event.getWorld().setBlockState(event.getPos(), type.getDefaultState());
                    }
                }
            }
        }

        if (heldItemStack.getItem().getToolTypes(heldItemStack).contains(state.getBlock().getHarvestTool(state))) {

            for (PathBlock type : PathBlock.BLOCKS.values()) {
                if (state.getBlock().equals(type.getParent())) {
                    event.getPlayer().swingArm(event.getHand());
                    event.getWorld().setBlockState(event.getPos(), type.getDefaultState());
                }
            }
        }

        if (Tags.Items.DUSTS_GLOWSTONE.contains(heldItemStack.getItem())) {

            if (state.getBlock() instanceof PathBlock && !(state.getBlock() instanceof GlowingPathBlock)) {
                try {
                    event.getWorld().setBlockState(event.getPos(), ForgeRegistries.BLOCKS.getValue(new ResourceLocation(state.getBlock().getRegistryName() + "_glowing")).getDefaultState());
                    heldItemStack.setCount(heldItemStack.getCount() - 1);
                    event.getPlayer().swingArm(event.getHand());
                } catch (Exception exception) {
                    event.getPlayer().sendMessage(new StringTextComponent("Failed to upgrade path: " + state.getBlock().getNameTextComponent() + " - Glowing state not found?"));
                    ExtraPathsMod.LOGGER.error(">>>Failed to upgrade path<<<", exception);
                }
            }

        }

    }


}
