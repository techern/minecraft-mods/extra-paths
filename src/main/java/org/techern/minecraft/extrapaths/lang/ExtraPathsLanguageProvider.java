package org.techern.minecraft.extrapaths.lang;

import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.LanguageProvider;
import org.techern.minecraft.extrapaths.ExtraPathsMod;
import org.techern.minecraft.extrapaths.blocks.GlowingPathBlock;
import org.techern.minecraft.extrapaths.blocks.PathBlock;

/**
 * A {@link LanguageProvider} for EVERYTHING in this mod
 *
 * @since 1.0.0
 */
public class ExtraPathsLanguageProvider extends LanguageProvider {

    /**
     * Creates a new {@link ExtraPathsLanguageProvider}
     *
     * @param gen The {@link DataGenerator}
     * @param locale The {@link String} defining the current locale
     *
     * @since 1.0.0
     */
    public ExtraPathsLanguageProvider(DataGenerator gen, String locale) {
        super(gen, "extrapaths", locale);
    }

    @Override
    protected void addTranslations() {
        //First we do the ItemGroup
        add(ExtraPathsMod.ITEM_GROUP.getTranslationKey(), "Extra Paths");

        //Then the blocks
        for (PathBlock block : PathBlock.BLOCKS.values()) {
            String parentTranslation = block.getParent().getNameTextComponent().getString();

            if (parentTranslation.endsWith("s"))
                parentTranslation = parentTranslation.substring(0, parentTranslation.length() - 1);

            if (block instanceof GlowingPathBlock)
                parentTranslation = "Glowing " + parentTranslation;

            add(block, parentTranslation + " Path");
        }

        //Then the glowing blocks
        for (GlowingPathBlock block : PathBlock.GLOWING_BLOCKS.values()) {
            String parentTranslation = "Glowing " + block.getParent().getNameTextComponent().getString();

            if (parentTranslation.endsWith("s"))
                parentTranslation = parentTranslation.substring(0, parentTranslation.length() - 1);

            add(block, parentTranslation + " Path");
        }

    }
}
