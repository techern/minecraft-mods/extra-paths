package org.techern.minecraft.extrapaths.blocks;

import net.minecraft.data.DataGenerator;
import net.minecraft.util.Direction;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.client.model.generators.ExistingFileHelper;
import net.minecraftforge.client.model.generators.ModelFile;

/**
 * A {@link BlockStateProvider} for all {@link PathBlock}s
 *
 * @since 1.0.0
 */
public class PathBlockStateProvider extends BlockStateProvider {

    /**
     * Creates a new {@link PathBlockStateProvider}
     *
     * @param generator The {@link DataGenerator}
     * @param fileHelper The {@link ExistingFileHelper}
     *
     * @since 1.0.0
     */
    public PathBlockStateProvider(final DataGenerator generator, final ExistingFileHelper fileHelper) {
        super(generator, "extrapaths", fileHelper);
    }

    /**
     * Registers all new {@link net.minecraft.block.BlockState} and {@link net.minecraft.client.renderer.model.BlockModel}s
     *
     * @since 1.0.0
     */
    @Override
    protected void registerStatesAndModels() {
        for (PathBlock block : PathBlock.BLOCKS.values()) {
            simpleBlock(block,
                    getBuilder(block.getRegistryName().getPath())
                            .parent(new ModelFile.UncheckedModelFile(mcLoc("block/block")))
                            .element().from(0, 0, 0).to(16, 15, 16)
                            .face(Direction.UP).uvs(0, 0, 16, 16).texture("#top").end()
                            .face(Direction.DOWN).uvs(0, 0, 16, 16).cullface(Direction.DOWN).texture("#bottom").end()
                            .face(Direction.NORTH).uvs(0, 1, 16, 16).cullface(Direction.NORTH).texture("#side").end()
                            .face(Direction.SOUTH).uvs(0, 1, 16, 16).cullface(Direction.SOUTH).texture("#side").end()
                            .face(Direction.EAST).uvs(0, 1, 16, 16).cullface(Direction.EAST).texture("#side").end()
                            .face(Direction.WEST).uvs(0, 1, 16, 16).cullface(Direction.WEST).texture("#side").end()
                            .end()
                            .texture("side", mcLoc(block.getSideTexture()))
                            .texture("particle", mcLoc(block.getSideTexture()))
                            .texture("top", mcLoc(block.getTopTexture()))
                            .texture("bottom", mcLoc(block.getBottomTexture())));

        }

        for (PathBlock block : PathBlock.GLOWING_BLOCKS.values()) {
            simpleBlock(block,
                    getBuilder(block.getRegistryName().getPath())
                            .parent(new ModelFile.UncheckedModelFile(mcLoc("block/block")))
                            .element().from(0, 0, 0).to(16, 15, 16)
                            .face(Direction.UP).uvs(0, 0, 16, 16).texture("#top").end()
                            .face(Direction.DOWN).uvs(0, 0, 16, 16).cullface(Direction.DOWN).texture("#bottom").end()
                            .face(Direction.NORTH).uvs(0, 1, 16, 16).cullface(Direction.NORTH).texture("#side").end()
                            .face(Direction.SOUTH).uvs(0, 1, 16, 16).cullface(Direction.SOUTH).texture("#side").end()
                            .face(Direction.EAST).uvs(0, 1, 16, 16).cullface(Direction.EAST).texture("#side").end()
                            .face(Direction.WEST).uvs(0, 1, 16, 16).cullface(Direction.WEST).texture("#side").end()
                            .end()
                            .texture("side", mcLoc(block.getSideTexture()))
                            .texture("particle", mcLoc(block.getSideTexture()))
                            .texture("top", mcLoc(block.getTopTexture()))
                            .texture("bottom", mcLoc(block.getBottomTexture())));

        }
    }
}
